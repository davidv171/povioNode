/*
isAuthenticated.js
Gives access to controllers, if there is a token, check the token validity inside a helper
Once the user is logged in, give him a JWT token which permits him
to use certain routes, JWT is then used to establish a session
*/

module.exports = async function(req,res,proceed){
  try{
    if(!req.headers['token']) return res.status(400).json({ err: e.name, info: 'Wrong token', code: 1749 });
    if(req.headers['token']) proceed();
  }catch(e){
    return res.status(40).json({ err: e.name, info: 'Wrong token', code: 1749 });
    }


}


