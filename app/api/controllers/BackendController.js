/**
 * BackendController
 *
 * @description :: Server-side actions for handling all incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var bcrypt = require('bcryptjs');
function checkInput(req, res) {
  //Check if POST method has both a username and password, saves time, so we don't go searching for blank spaces
  if (req.body.username && req.body.password) {
    var userData = {
      username: req.body.username,
      password: req.body.password
    }
    if (!isNaN(userData.username.charAt(0)) || !isNaN(userData.password.charAt(0))
      || userData.username.length < 4 || userData.password.length < 4 ||
      userData.username.length > 32 || userData.password.length > 32) {
      return res.status(400).json({ err: 'Invalid input', status: 'Password or username invalid', code: 1739 });
    }

  }
  else {
    return res.status(400).json({ err: 'Empty body', info: 'POST request body empty', code: 1743 });
  }

}
//The same method, that only looks out for the password, is separated from checkInput for simplicity and debugging's sake
function checkPUT(req, res) {
  if (req.body.password) {
    var userData = {
      password: req.body.password
    }
    if (!isNaN(userData.password.charAt(0))
      || userData.password.length < 4 ||
      userData.password.length > 32) {
      return res.status(400).json({ err: 'Invalid input', status: 'Password or username invalid', code: 1739 });
    }
  }
  else {
    return res.status(400).json({ err: 'Empty body', info: 'PUT request body empty', code: 1743 });
  }
}
module.exports = {
  //For clarity: req = request, res = response
  signup: async function (req, res) {
    //Check if empty POST body

    if (!checkInput(req, res)) {
      //Check if username exists, if not try creating a new user!

      var userData = {
        username: req.body.username,
        password: req.body.password
      }
      try {
        const foundUser = await User.findUser(userData.username);
        if (foundUser) return res.status(500).json({ err: e.name, info: 'An error with the database adapter,user most likely exists', code: 1741 });
        //Hashing hapens in User.js as part of the waterline cycle(beforeCreate)
        const createdUser = await User.create({ username: userData.username, password: userData.password });
        return res.status(200).json({ status: 'success', result: createdUser });
      } catch (e) {
        if (e.name === 'ReferenceError') return res.status(400).json({ err: e.name, info: 'User most likely already exists', code: 1749 });
        if (e.name === 'UsageError') return res.status(400).json({ err: e.name, info: 'Something invalid was passed in', code: 1740 });
        if (e.name === 'AdapterError') return res.status(500).json({ err: e.name, info: 'An error with the database adapter,user most likely exists', code: 1741 });
        if (e.name === 'Error') return res.status(500).json({ err: e.name, info: 'Something unexpected happened', code: 1742 });
      }
    }
  },
  login: async function (req, res) {
    //Check if valid input
    if (!checkInput(req, res)) {
      var userData = {
        username: req.body.username,
        password: req.body.password
      }
      //Compare hashed inputted password and password inside the usernames, given that the username exists
      try {

        const foundUser = await User.findUser(userData.username);
        hashedPass = foundUser.password;
        bcrypt.compare(userData.password, hashedPass, async function (err, compareRes) {
          if (compareRes) {
            //Signed with HMAC SHA256 by default
            var jwt = await sails.helpers.signjwt(foundUser.id, foundUser.username);
            return res.status(200).json({ status: 'Logged in', token: jwt });
          }
          else {
            return res.status(401).json({ err: 'Wrong credentials', info: 'User with these credentials does not exist', code: 1744 });

          }
        });
      } catch (e) {
        if (e.name === 'ReferenceError') return res.status(500).json({ err: 'User is not defined', info: 'User does not exist', code: 1742 });
        if (e.name === 'TypeError') return res.status(401).json({ err: 'Wrong credentials', info: 'User with these credentials does not exist', code: 1744 });
        if (e.name === 'UsageError') return res.status(400).json({ err: e.name, info: 'Something invalid was passed in', code: 1740 });
        if (e.name === 'AdapterError') return res.status(500).json({ err: e.name, info: 'An error with the database adapter,user most likely exists', code: 1741 });
        if (e.name === 'Error') return res.status(500).json({ err: e.name, info: 'Something unexpected happened', code: 1742 });
      }
    }
  },
  me: async function (req, res) {
    //Print out the currently logged in user's information using his jwt
    //First we find a user we want to find info on
    //Then we find Likes that are connected to the user that wants to like someone, using "source" attribute
    //Once its found print out the usernames of these likes using target_username
    try {
      var jwt = req.headers['token'];
      const source = await sails.helpers.getid(jwt);
      if (!source) return res.status(400).json({ err: e.name, info: 'Something invalid was passed in', code: 1740 });
      const foundUser = await User.findUser(source);
      const foundLikes = await Like.find({ source: foundUser.id });
      var likesArray = [];
      for (var i = 0; i < foundLikes.length; i++) {
        likesArray[i] = foundLikes[i].target_username;
      }
      return res.status(200).json({ status: 'Found user', id: foundUser.id, username: foundUser.username, likes: likesArray });
    }
    catch (e) {
      if (e.name === 'ReferenceError') return res.status(500).json({ err: 'User is not defined', info: 'User does not exist', code: 1742 });
      if (e.name === 'ImplementationError') return res.status(400).json({ err: e.name, info: 'Wrong token', code: 1749 });
      if (e.name === 'UsageError') return res.status(400).json({ err: e.name, info: 'Something invalid was passed in', code: 1740 });
      if (e.name === 'AdapterError') return res.status(500).json({ err: e.name, info: 'An error with the database adapter,user most likely doesnt exist', code: 1741 });
      if (e.name === 'Error') return res.status(500).json({ err: e.message, info: 'Something unexpected happened', code: 1742 });
    }

  },
  
  updatePassword: async function (req, res) {
    //Check for valid input
    if (!checkPUT(req, res)) {
      var userData = {
        username: req.body.username,
        password: req.body.password
      }
      //Find the user that wants to change the password using a helper method getid
      //Once we have the user's ID we find the user that wants to change his password
      //Then we update that user's password
      //Passowrd is hashed on beforeUpdate as part of the Waterline lifecycle
      try {
        var newPass = req.body.password;
        var jwt = req.headers['token'];
        const source = await sails.helpers.getid(jwt);
        const foundUser = await User.findUser(source);
        const updateUser = await User.update({ id: foundUser.id }).set({ password: newPass });
        return res.status(200).json({ status: 'success', updated: foundUser.username });

      } catch (e) {
        if (e.name === 'ReferenceError') return res.status(500).json({ err: 'User is not defined', info: 'User does not exist', code: 1742 });
        if (e.name === 'ImplementationError') return res.status(400).json({ err: e.name, info: 'Wrong token', code: 1749 });
        if (e.name === 'UsageError') return res.status(400).json({ err: e.name, info: 'Something invalid was passed in', code: 1740 });
        if (e.name === 'AdapterError') return res.status(500).json({ err: e.name, info: 'An error with the database adapter', code: 1741 });
        if (e.name === 'Error') return res.status(500).json({ err: e.message, info: 'Something unexpected happened', code: 1742 });
      }

    }
  },
  list: async function (req, res) {
    const value = req.param('username');
    //List data about user, based on id or username in GET request, ID searches start with a number, other searches start with varter
    //Find a user based on the id or username we put in
    //Get the user's likes by searching through the Like table with the searched for ID as source
    try {
      const foundUser = await User.findUser(value);
      const foundLikes = await Like.find({ source: foundUser.id });
      var likesArray = [];
      for (var i = 0; i < foundLikes.length; i++) {
        likesArray[i] = foundLikes[i];
      }
      res.status(200).json({ status: 'success', username: foundUser.username, likes: likesArray.length, likedByCount: foundUser.likedByCount });
    } catch (e) {
      (e.name);
      if (e.name === 'ReferenceError') return res.status(500).json({ err: 'User is not defined', info: 'User does not exist', code: 1742 });
      if (e.name === 'TypeError') return res.status(401).json({ err: 'Wrong credentials', info: 'User with these credentials does not exist', code: 1744 });
      if (e.name === 'UsageError') return res.status(400).json({ err: e.name, info: 'Something invalid was passed in', code: 1740 });
      if (e.name === 'AdapterError') return res.status(500).json({ err: e.name, info: 'An error with the database adapter', code: 1741 });
      if (e.name === 'Error') return res.status(500).json({ err: e.message, info: 'Something unexpected happened', code: 1742 });
    }

  },
  like: async function (req, res) {
    //First we get the jwt out of the headers, use that to find the ID of the user that wants to like someone
    //Using the value from the HTTP GET Request URL we find the target user
    //Target user is either found through his username
    //Create a new entry or in case it exists, just return it of the Like table
    //Target = people who is going to be liked
    //Source = person that did the liking, the currently logged in user
    var target = req.param('username');
    var jwt = req.headers['token'];
    try {
      const source = await sails.helpers.getid(jwt);
      const target_user = await User.findUser(target);
      target = target_user.id;
      const target_username = target_user.username;
      const source_user = await User.findOne({ id: source });
      const source_username = source_user.username;
      //In case the user that is currently logged in is the same as the user he's trying to like 
      if (source === target) {
        return res.status(200).json({ err: 'Liking yourself error', info: 'You cant like yourself', code: 1747 });
      }
      else {
        const userWithLikes = await Like.findOrCreate({ target: target, source: source, target_username: target_username, source_username: source_username }, { target: target, source: source, target_username: target_username, source_username: source_username }, function (err, newOrExistingRecord, wasCreated) {
          if (err) res.status(500).json({ err: err.name, info: err.message });//error
          if (wasCreated) return res.status(200).json({ status: 'Successfully liked' });        //created a new entry
          if (newOrExistingRecord) return res.status(400).json({ err: 'Already liked error', info: 'You have already liked this person', code: 1745 });//found the entry and updated it
        });
      }


    }
    catch (e) {
      if (e.name === 'ReferenceError') return res.status(500).json({ err: 'User is not defined', info: 'User does not exist', code: 1742 });
      if (e.name === 'AdapterError') return res.status(500).json({ err: e.name, info: 'An error with the database adapter', code: 1741 });
      if (e.name === 'ImplementationError') return res.status(400).json({ err: e.name, info: 'Wrong token', code: 1749 });
      if (e.name === 'Error') return res.status(500).json({ err: e.message, info: 'Something unexpected happened', code: 1742 });
      if (e.name === 'TypeError') return res.status(401).json({ err: e.name, info: 'An error with the database adapter,user most likely doesnt exist', code: 1741 });
    }

  },
  unlike: async function (req, res) {
    //Delete an entry from the like table
    //Find the user that wants to unlike someone using his JWT and find the user that is going to be unliked
    //Through the HTTP GET request URL
    try {
      var target = req.param('username');
      var jwt = req.headers['token'];
      const source = await sails.helpers.getid(jwt);
      const foundUser = await User.findUser(target);
      target = foundUser.id;
      //In case the user wants to unlike himself(id from jwt is the same as the id from URL)
      if (source === target) {
        return res.status(200).json({ err: 'Unliking yourself error', info: 'You cant unlike yourself', code: 1748 });
      }

      else {
        //Find a like using the target(from URL) and source(from jwt) id 
        //If found delete it 
        //If not found->reference error is thrown
        const foundLike = await Like.findOne({ target: foundUser.id, source: source });
        if (!foundLike) return res.status(400).json({ err: 'Already unliked error', info: 'You have already unliked this person', code: 1746 });
        const userWithLikes = await Like.destroy({ id: foundLike.id }).fetch();
        if (userWithLikes) return res.status(200).json({ status: 'Successfully unliked' });
      }

    } catch (e) {
      if (e.name === 'ReferenceError') return res.status(500).json({ err: 'User is not defined', info: 'User does not exist', code: 1742 });
      if (e.name === 'AdapterError') return res.status(500).json({ err: e.name, info: 'An error with the database adapter', code: 1741 });
      if (e.name === 'ImplementationError') return res.status(400).json({ err: e.name, info: 'Wrong token', code: 1749 });
      if (e.name === 'Error') return res.status(500).json({ err: e.message, info: 'Something unexpected happened', code: 1742 });
      if (e.name === 'TypeError') return res.status(401).json({ err: e.name, info: 'An error with the database adapter,user most likely doesnt exist', code: 1741 });
    }



  },
  mostLiked: async function (req, res) {
    //list users from most liked to least liked, based on their likedByCount value
    //goes through every user
    try {
      var users = await User.find({})
        .sort('likedByCount desc');
      var leaderBoard = [];
      for (var i = 0; i < users.length; i++) {
        leaderBoard[i] = { id: users[i].id, username: users[i].username, numberOfLikes: users[i].likedByCount };
      }
      return res.status(200).json({ status: 'Success', leaderBoard: leaderBoard });
    }

    catch (e) {
      if (e.name === 'ReferenceError') return res.status(500).json({ err: 'User is not defined', info: 'User does not exist', code: 1742 });

    }
  }



};

