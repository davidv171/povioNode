module.exports = {


  friendlyName: 'Get id',


  description: 'Get an ID that identifies a user from a JWT ',


  inputs: {
    jwt:{type:'string'}
  },


  exits: {

    success: {
      outputFriendlyName: 'Id',
      outputType: 'ref'
    },

  },


  fn: async function (inputs, exits) {
    // Get id.
    var jsonWeb = require('jsonwebtoken');
    try {
      var decoded = jsonWeb.verify(inputs.jwt,sails.config.session.secret);
      var id = decoded.id;
      // TODO
      // Send back the result through the success exit.
      return exits.success(id);
    } catch (e) {
      return exits.error(e);
    }


  }


};

