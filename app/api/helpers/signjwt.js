module.exports = {


  friendlyName: 'sign jwt',


  description: 'Get payload and sign JWT ',


  inputs: {
    id:{type:'number'},
    username:{type:'string'}
  },


  exits: {

    success: {
      outputFriendlyName: 'Id',
      outputType: 'ref'
    },

  },


  fn: async function (inputs, exits) {
    try {
      var jsonWeb = require('jsonwebtoken');
      var jwt = jsonWeb.sign(
        {id:inputs.id,username:inputs.username},
        sails.config.session.secret,
        {
          expiresIn:"7d"
         }
       )
       return exits.success(jwt);
    } catch (e) {
      return exits.error(e);

    }
  }


};

