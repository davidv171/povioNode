var bcrypt = require('bcryptjs');
module.exports = {
  attributes: {
    id: {type:'number',autoIncrement:true,unique:true},
    username: {type:'string',required:true,unique:true},
    password: {type:'string',required:true,protect:true,unique:true},
    likedByCount:{type:'number',defaultsTo:0},
    Like: {
      collection: 'Like',
      via: 'source'
    }

  },
  //Find user based on its key(username/id) and the value of the key)
  //Sometimes value is a string sometimes a number!!!
  findUser:async function(value){
    var idValue = null;

    try{
      if (!isNaN(value.charAt(0))){
        idValue = value;
      }
    }
    catch(e){
      idValue = value;
    }
      //search for id/username based on input
    return await User.findOne({
      or:[
        {username:value},
        {id:idValue}
      ]
    });

  },

  beforeCreate: function(valuesToSet,proceed){
    try{
      const passwordToHash = valuesToSet.password;
      const hashedPass = bcrypt.hashSync(passwordToHash,bcrypt.genSaltSync(8), null);
      valuesToSet.password = hashedPass;
      return proceed();
    }catch(e){
      return proceed(e);
    }

  },
  beforeUpdate: function(valuesToSet,proceed){
    if(!valuesToSet.password){
      return proceed();
    }
    try{
      const passwordToHash = valuesToSet.password;
      const hashedPass = bcrypt.hashSync(passwordToHash, bcrypt.genSaltSync(8), null);
      valuesToSet.password = hashedPass;
      return proceed();
    }catch(e){
      return proceed(e);
    }


  }




};
