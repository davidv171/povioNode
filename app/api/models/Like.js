module.exports={
  attributes:{
    id: {type:'number',autoIncrement:true,unique:true},
    //Person that the target was liked by is the owner->the person that did the liking
    target_username:{type:'string'},
    source_username:{type:'string'},
    target: {type:'number'},
    source: {
      model: 'User'
    },


  },
  beforeCreate: async function(valuesToSet,proceed){
    //Find the target user using target, get his liked_by number and increase it by 1
    try {
      const valueTargetId = valuesToSet.target;
      const foundTarget = await User.findOne({id:valueTargetId});
      let foundTargetLikedBy = foundTarget.likedByCount;
      foundTargetLikedBy = foundTargetLikedBy+1;
      const updatedUser = await User.update(foundTarget).set({likedByCount:foundTargetLikedBy}).fetch();
      return proceed();
    } catch (error) {
      return proceed(error);
    }
  },
  afterDestroy: async function(destroyedRecord,proceed){
    //Decrement the target that was unliked
    try {
      const valueTargetId = destroyedRecord.target;
      const foundTarget = await User.findOne({id:valueTargetId});
      let foundTargetLikedBy = foundTarget.likedByCount;
      foundTargetLikedBy = foundTargetLikedBy-1;
      const updatedUser = await User.update(foundTarget).set({likedByCount:foundTargetLikedBy}).fetch();
      return proceed();
    } catch (error) {
      return proceed(error);
    }

  }


};
