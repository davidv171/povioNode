# povioNode

A backend application going through the basics of a REST node.js API

## Guide through the code:

Most of non-Sails generated code is written in:
- api/controllers/BackendController.js
    - Most methods are here, logic for responses to API routes
- api/helpers/getid.js
    - A helper written to get an id out of a JWT signed toked from anywhere
- api/helpers/signjwt.js
    - A helper written to sign an JWT out of the parsed payload
- api/models/Like.js
    - A model defining the "Like" table, which contains:id,target_username,source_username,target,source(foreign key of User) 
- api/models/User.js
    - A model defining the "User" table, which contains: id, username,password,likedByCount and a reference to Like
- api/policies/isToken.js
    - A policy, that does not allow the BackendController execute a method, if the HTTP request does not contain a JWT, if the call is authenticated
- api/config/datastores.js
    - Contains the database config, currently is just a link to an ElephantSQL database
- api/config/routes.js
    - Contains the definitions of REST endpoints and their corresponding methods in BackendController, that are executed

## Run with:

- Install sails globally using npm

npm install sails

Since you're probably missing most dependencies the easiest way is to navigate to:

cd ~/source-folder/app 

and run:

npm install

- Navigate to source folder

cd ~/source-folder/app

- Start the application using node:

node app.js

- (optional) run tests with postman using the "runTest.postman_collection.json" file in the root of the repository


## Tools and technologies:

- Node.js v8(async/await functionality)
- Postgresql(to be determined)
- Sails.js
- git
- json
- body-parser for parsing request bodies
- bcrypt for easily hashing and salting passwords
- nodemon which basically restarts the command on file change!
- jwt token authentication
- a tool for testing

## Dependencies:

- bcrypt
- sails-postgre
- jsonwebtoken

## REST endpoints:

### POST /signup

Sign up to the system(username and password). 

#### Request:

POST:

- username: username
- password: password

##### Request rules

- Username and password cannot start with a number
    - response: ```400 {err:'Invalid input',status:'Password or username invalid',code:1739}```
- Username and password must be longer than 4, shorter than 32"
    - response: ```400 {err:'Invalid input',status:'Password or username invalid',code:1739}```
- Username must not already exist in the entry or general error with the database
    - response: ```500 {err:e.name,info:'An error with the database adapter, user most likely exists',code:1741}```

Invalid input errors take priority over errors with existing usernames, because the server internally checks the input before querying the database.

##### Successful output

In case you have successfuly signed up the user, it generates the following response:
```
200 {status:'success',result:createdUser}
```

### POST /login

Log in an existing user with a password

#### POST request body:


- username: username
- password: password

##### Request rules

- POST body must not be empty
    - 400 ```{err:'Empty body',info:'POST request body empty',code:1743}```
- User credentials must be correct
    - 400 ```{err:'Wrong credentials',info:'User with these credentials does not exist',code:1744}```

##### Successful output

In case you have successfuly signed in the user, it generates the following response:

200 
```{status:'Logged in',token:jwt}```
  


### GET /me

Get the currently logged in user information. This call needs to be authenticated.

#### Request headers

- token : jwt

JWT: JSON Web Token you received when logging in. Token times out after **7 days**

### PUT /me/update-password

Change the logged in user's password.

### PUT Request body

- password : password

Same rules apply and error outputs apply as during signup.

#### Successful response

Example of a successful call:

http://127.0.0.1:1337/api/me/update-password

Response:

``` json

{
    "status": "success",
    "updated": "test2"
}


```

### /user/:id/

:id could be a user's username or a users id.

Lists the usernames and number of likes of a user. This is not an authenticated call, which is why the body can be empty.

Example of a request:

http://127.0.0.1:1337/api/10

or 

http://127.0.0.1:1337/api/david14

#### Successful response

``` json
{
    "status": "success",
    "username": "david14",
    "likes": 0,
    "likedByCount": 2
}
```
In which case the "likes" is the amount of people the user in question has liked.

"LikedByCount" is the amount of people that have been liked

### GET /user/:id/like

Likes the user in question. This call needs to be authenticated

Example:

/user/davidv171/like

This means the user that's logged in likes the user davidv171. 

Each user can like another user only once. 

#### GET request headers

Example of a good call:

http://127.0.0.1:1337/api/user/1/like

or 

http://127.0.0.1:1337/api/user/test2/like

GET Headers:

- token : jwt

#### Request rules

- You can't like yourself:
```json
{
    "err": "Liking yourself error",
    "info": "You cant like yourself",
    "code": 1747
}
```
- You can only like a existing user 

```json
{
    "err": "TypeError",
    "info": "An error with the database adapter,user most likely doesnt exist",
    "code": 1741
}
```
- You can't like a user you've already liked
```json
{
    "err": "Liking yourself error",
    "info": "You cant like yourself",
    "code": 1747
}
```
#### Successful response


```json
{
    "status": "Successfully liked"
}
```

### /user/:id/unlike

Unlike a user that the logged in user has previously liked.

#### GET request headers

Example of a good call:

http://127.0.0.1:1337/api/user/1/unlike

or 

http://127.0.0.1:1337/api/user/test2/unlike

GET Headers:

- token : jwt

#### Request rules

- You can't unlike yourself:
```json
{
    "err": "Liking yourself error",
    "info": "You cant like yourself",
    "code": 1747
}
```
- You can only unlike an existing user 

```json
{
    "err": "TypeError",
    "info": "An error with the database adapter,user most likely doesnt exist",
    "code": 1741
}
```
- You can't unlike a user you've already liked
```json
{
"err":"Already unliked error",
"info":"You have already liked this person",
"code":1746
}
```
#### Successful response

```json
{
    "status": "Successfully liked"
}
```

### GET /most-liked

List users in a most to least liked manner. 

Is not an authenticated calls and does not need any authentication.

Example:

http://127.0.0.1:1337/api/most-liked

#### Successful response

```json
{
    "leaderBoard": [
        {
            "id": 10,
            "username": "david14",
            "numberOfLikes": 2
        },
        {
            "id": 1,
            "username": "test",
            "numberOfLikes": 1
        },
        {
            "id": 4,
            "username": "test420",
            "numberOfLikes": 0
        },
        {
            "id": 5,
            "username": "test2",
            "numberOfLikes": 0
        },
        {
            "id": 6,
            "username": "test251",
            "numberOfLikes": 0
        },
        {
            "id": 7,
            "username": "test25115",
            "numberOfLikes": 0
        },
        {
            "id": 8,
            "username": "david",
            "numberOfLikes": 0
        },
        {
            "id": 9,
            "username": "david1",
            "numberOfLikes": 0
        },
        {
            "id": 11,
            "username": "david145",
            "numberOfLikes": 0
        },
        {
            "id": 12,
            "username": "david145151515151",
            "numberOfLikes": 0
        },
        {
            "id": 14,
            "username": "david1451515151514161",
            "numberOfLikes": 0
        },
        {
            "id": 15,
            "username": "david1451515151514161141414141",
            "numberOfLikes": 0
        },
        {
            "id": 16,
            "username": "test15161",
            "numberOfLikes": 0
        },
        {
            "id": 17,
            "username": "yaz21161",
            "numberOfLikes": 0
        },
        {
            "id": 18,
            "username": "gqa721",
            "numberOfLikes": 0
        },
        {
            "id": 19,
            "username": "vyq62",
            "numberOfLikes": 0
        },
        {
            "id": 20,
            "username": "david41",
            "numberOfLikes": 0
        },
        {
            "id": 2,
            "username": "test1",
            "numberOfLikes": 0
        }
    ]
}
```
## Tests

Postman takes care of 47 tests for us. They're not really automatic, they were more used to test robus


## TODOs:
- User registration(first) /
- User authentication /
- JWT token authentication /
- Set up a database /
- Changing database parameters(update passwords,like,unlike) /
- Automatic postman tests





